# Biometric template samples

This repo contains sets of biometric templates to be used in simulating a biometric system.

These sets were obtained by running open-source biometric datasets through open source biometric recognition networks. Links to the source data and code/model are found in each item of the list below.

# How to load

Just load the selected `.npy` file in python using `np.load("...")`, or alternatively in C++ using [cnpy](https://github.com/rogersce/cnpy).

Each folder contains files whose names begin with the dataset name. 
There is one file with the label of identity of each biometric sample (in strict order), and one/several files with biometric templates obtained by running a selected biometric recognition model on a source biometric sample.

The naming convention is `{dataset}_{labels|embeddings}_{n_template_vals}{val_type, f:float | i:integer}_{model_name}.npy`

# List of templates

## Face

- [LFW dataset](http://vis-www.cs.umass.edu/lfw/): 13232 faces, 112x112px Aligned.
	- `LFW_embeddings_512f_insightface_R100.npy`: Templates obtained with the [insightface PartialFC Pytorch R100 model](https://github.com/deepinsight/insightface/tree/master/recognition/partial_fc/#5-pretrain-models).
